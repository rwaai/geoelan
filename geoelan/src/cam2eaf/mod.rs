//! Locate video-files (GoPro, Garmin VIRB) and FIT (Garmin VIRB), and generate an ELAN-file.

pub mod gopro2eaf;
pub mod virb2eaf;
